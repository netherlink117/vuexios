# Vuexios
A Vue plugin to work with axios within the Vue instance. It is a wrapper for the axios package found in npmjs.
## Install
I'm so lazy to make a public package, so just clone this into the project folder (commonly node_modules but there must not be already any vuexios folder) and there you are.
### Start
This is how you import vuexios when you clone it into the node_modules folder. Options is the parameter for the createInstance function, and is an object with the same possible settings as the create function from axios package from npmjs.
```javascript
...
import { createInstance } from "vuexios";
const options = {
    ...
};
let axios = createInstance(options);
createApp(App)
  .use(axios)
  .mount("#app");
...
```
### Usage
You can use the vuexios inside any component like any other plugin
```javascript
...
this.$axios.get(...);
this.$axios.post(...);
this.$axios.put(...);
// and etc....
...
```
For more examples on the axios instance (and functions), please check the documentation for the current axios package at npmjs.
## License
The code shared on this repository is shared under [MIT](https://opensource.org/licenses/MIT) license.
