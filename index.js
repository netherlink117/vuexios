export const axios = {
    install(_app, _opts) {
      if (_opts === undefined || _opts === null) {
        throw "axios options not defined.";
      }
      let _axios = require("axios");
      let _axios_instance = _axios.create(_opts);
      _app.config.globalProperties.$axios = _axios_instance;
    }
  };
  
  export const createInstance = _options => {
    if (_options === undefined || _options === null) {
      throw "axios options not defined.";
    }
    return {
      install(_app) {
        let _axios = require("axios");
        let _axios_instance = _axios.create(_options);
        _app.config.globalProperties.$axios = _axios_instance;
      }
    };
  };
  
  export default {
    install(_app, _opts) {
      if (_opts === undefined || _opts === null) {
        throw "axios options not defined.";
      }
      let _axios = require("axios");
      let _axios_instance = _axios.create(_opts);
      _app.config.globalProperties.$axios = _axios_instance;
    }
  };
  
